def foo():
    print('This should be printed from a')


if __name__ == '__main__':
    print("This should not be printed from a #2")
    print("This should not be printed from a #1")